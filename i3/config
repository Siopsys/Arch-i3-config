# i3 config file (v4)
# Please see http://i3wm.org/docs/userguide.html for a complete reference!

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:Linux Libertine 8

# Mod Set:
set $mod Mod4

# smart gaps
smart_gaps on

# WS Set:
set $ws1 "1: Term"
set $ws2 "2: Vivaldi"
set $ws3 "3: Chat"
set $ws4 "4: Media"
set $ws5 "5: Code"
set $ws6 "6: Other"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# Assign Windows:
assign [class="termite"] $ws1
#assign [class="Thunar"] $ws2
assign [class="vivaldi"] $ws2
#assign [class="Steam"] $ws4
#assign [class="Spotify"] $ws4

# Set Colors:
set $black 	#212121
set $pureblack	#000000
set $yellow	#ECCA54
set $blue	#5FB9ED
set $purple	#775FED
set $darkred	#32140A
set $darkgreen	#0A332D
set $darkblue	#0A1833

# use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec termite

# kill focused window
bindsym $mod+q kill

# start rofi (a program launcher)
bindsym $mod+d exec --no-startup-id rofi -show 
# start rofi window switcher
bindsym $mod+w exec --no-startup-id rofi -show window

# Window Focus
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Move Window
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+t layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# move the currently focused window to the scratchpad
#bindsym $mod+Shift+minus move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
#bindsym $mod+minus scratchpad show

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e mode "$exit"
set $exit (l)ock, (e)xit, (s)hutdown, (r)eboot
mode "$exit" {
	bindsym l exec --no-startup-id i3lock, mode "default"
	bindsym e exec --no-startup-id i3-msg exit, mode "default"
	bindsym s exec termite -e shutdown now
	bindsym r exec termite -e reboot
}

# Windows:
new_window normal 0 px
for_window [class="^.*"] border pixel 4
gaps inner 6
gaps outer 2

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the windows width.
        # Pressing right will grow the windows width.
        # Pressing up will shrink the windows height.
        # Pressing down will grow the windows height.
        bindsym $left       resize shrink width 10 px or 10 ppt
        bindsym $down       resize grow height 10 px or 10 ppt
        bindsym $up         resize shrink height 10 px or 10 ppt
        bindsym $right      resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left        resize shrink width 10 px or 10 ppt
        bindsym Down        resize grow height 10 px or 10 ppt
        bindsym Up          resize shrink height 10 px or 10 ppt
        bindsym Right       resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
        status_command i3status --config ~/.config/i3/i3status
	font pango:Linux Libertine 10
        tray_output primary
	position top
	workspace_buttons yes
	separator_symbol "]["
	tray_padding 0
	colors {
		# Class			Border		Background	Text
		focused_workspace	$darkred	$yellow		$pureblack
		active_workspace	$yellow		$blue		$purple
		inactive_workspace	$pureblack	$darkred	$blue
		urgent_workspace	$blue		$blue		$purple
		
		statusline		$blue
		background		$pureblack
		separator		$yellow
	}
}

# Colors:
# Class			Border		Background	Text		Indicator
client.focused		$darkred	$darkred	$purple		$darkblue
client.unfocused	$black		$pureblack	$blue		$darkblue
client.focused_inactive $black		$pureblack	$blue		$darkblue
client.urgent		$blue		$blue		$purple		$darkred
client.background	$black

# Screenshot
#bindsym $mod+Shift+i exec --no-startup-id termite -e scrot

# Programs:
#bindsym $mod+Shift+g exec --no-startup-id google-chrome-stable
#bindsym $mod+Shift+t exec --no-startup-id thunar
#bindsym $mod+Shift+s exec --no-startup-id steam
#bindsym $mod+Shift+p exec --no-startup-id spotify

# Thinkpad Keys:
# Pulse Audio controls
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +5% #increase sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5% #decrease sound volume
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle # mute sound

# Sreen brightness controls
bindsym XF86MonBrightnessUp exec xbacklight -inc 20 # increase screen brightness
bindsym XF86MonBrightnessDown exec xbacklight -dec 20 # decrease screen brightness

# Touchpad controls
bindsym XF86TouchpadToggle exec /some/path/toggletouchpad.sh # toggle touchpad

# Media player controls
bindsym XF86AudioPlay exec playerctl play
bindsym XF86AudioPause exec playerctl pause
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous

# Wallpaper
exec --no-startup-id feh --bg-scale ~/Wallpaper/current.jpg

# Set Keymap on start
exec "setxkbmap -layout gb"

# Startups:
#exec thunar --daemon
exec --no-startup-id pulseaudio
